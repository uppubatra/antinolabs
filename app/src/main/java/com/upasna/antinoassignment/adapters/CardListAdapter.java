package com.upasna.antinoassignment.adapters;

import android.content.Context;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import androidx.cardview.widget.CardView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.upasna.antinoassignment.R;
import com.upasna.antinoassignment.models.ModelCardData;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;



public class CardListAdapter extends RecyclerView.Adapter<CardListAdapter.MyViewHolder> {

    private List<ModelCardData> dataList;
    private Context context;

    public CardListAdapter(Context context, List<ModelCardData> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView civProfile;
        TextView tvName,tvAge,tvLocation;

        CardView cvRoot;
        MyViewHolder(View view) {
            super(view);
            cvRoot = view.findViewById(R.id.cvRoot);
            tvName = view.findViewById(R.id.tvName);
            tvAge = view.findViewById(R.id.tvAge);
            tvLocation = view.findViewById(R.id.tvLocation);
            civProfile = view.findViewById(R.id.civProfile);



        }
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final ModelCardData dataModel = dataList.get(position);
        holder.tvName.setText(dataModel.getName());
        holder.tvAge.setText(dataModel.getAge());
        holder.tvLocation.setText(dataModel.getLocation());


        Picasso.get().load(dataModel.getUrl()).into(holder.civProfile);





    }

    @Override
    public int getItemCount() {
        return this.dataList.size();
    }
}




