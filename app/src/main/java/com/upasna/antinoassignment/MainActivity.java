package com.upasna.antinoassignment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.upasna.antinoassignment.adapters.CardListAdapter;
import com.upasna.antinoassignment.callbacks.ApiCallBacks;
import com.upasna.antinoassignment.constants.ApiEndpoints;
import com.upasna.antinoassignment.constants.ApiTags;
import com.upasna.antinoassignment.managers.ApiManager;
import com.upasna.antinoassignment.models.ModelCardData;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ApiCallBacks {


    private RecyclerView recyclerView;
    private List<ModelCardData> list=new ArrayList<>();
    private CardListAdapter mAdapter;
    private ProgressDialog mProgressDialog=null;
    private SwipeRefreshLayout pullToRefresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Fetching Data...");
        initViews();

        initListeners();


    }



    private void initViews() {

        pullToRefresh = findViewById(R.id.pullToRefresh);
        recyclerView =  findViewById(R.id.recycler_view);

    }

    private void initListeners() {

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callApi();
                pullToRefresh.setRefreshing(false);
            }
        });
    }




    @Override
    protected void onResume() {
        super.onResume();

        callApi();
    }


    private void callApi(){
        if(!mProgressDialog.isShowing()) mProgressDialog.show();
        ApiManager.callGetApi(this,this, ApiTags.CARD_DATA_API, ApiEndpoints.CARD_DATA_API);

    }

    @Override
    public void onSuccess(String response) {

        if(mProgressDialog.isShowing()) mProgressDialog.dismiss();

        //parsing json array in Array List of Model Class
        Type listType = new TypeToken<List<ModelCardData>>(){}.getType();
         list =   new Gson().fromJson(response, listType);


        //recycler view adapter init
        try{
            mAdapter = new CardListAdapter(this,list);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }catch (Exception e){
            Toast.makeText(this, ""+e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onError(String error) {
        if(mProgressDialog.isShowing()) mProgressDialog.dismiss();
        Toast.makeText(this, ""+error, Toast.LENGTH_SHORT).show();
    }
}
