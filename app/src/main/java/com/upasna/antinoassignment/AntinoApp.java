package com.upasna.antinoassignment;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;

public class AntinoApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();


        //initializing the Android Networking Library
        AndroidNetworking.initialize(getApplicationContext());

    }
}
