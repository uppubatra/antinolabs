package com.upasna.antinoassignment.managers;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.upasna.antinoassignment.callbacks.ApiCallBacks;
import com.upasna.antinoassignment.utils.AppUtils;


public class ApiManager {


    public static void callGetApi(Context context, final ApiCallBacks apiCallBacks, final String apiTag, String apiUrl ){

        LogManager.logD("****"+apiTag+"****",apiUrl);
        if(!AppUtils.isNetworkConnected(context)){
            LogManager.logD("****"+apiTag+"****","No Internet Connection");
            apiCallBacks.onError("No Internet Connection Available");
        }else{
            LogManager.logD("****GET_API_URL::****","Fetching Data...");
            AndroidNetworking.get(apiUrl)
                    .setTag(apiTag)
                    .setPriority(Priority.LOW)
                    //.getResponseOnlyIfCached()
                    .build()
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String response) {
                            LogManager.logD("****Data Fetch:::"+apiTag+"*****",""+response);
                            apiCallBacks.onSuccess(response);
                        }

                        @Override
                        public void onError(ANError anError) {
                            LogManager.logD("****Error :::"+apiTag+"*****",""+anError.getLocalizedMessage());
                            apiCallBacks.onError(anError.getLocalizedMessage());
                        }
                    });
        }

    }
}
